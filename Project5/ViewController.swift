//
//  ViewController.swift
//  Project5
//
//  Created by Роман Хоменко on 31.03.2022.
//

import UIKit

class ViewController: UITableViewController {
    var allWords = [String]()
    var usedWords = [String]()
    
    var lastWord: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promptForAnswer))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(newGame))
        
        fetchWords()
        
        startGame()
    }
    
    func startGame() {
        let defaults = UserDefaults.standard
        if let savedWord = defaults.string(forKey: "word"), let savedUsedWords = defaults.array(forKey: "usedWords") as? [String] {
            lastWord = savedWord
            usedWords = savedUsedWords
        } else {
            lastWord = allWords.randomElement() ?? "silkworm"
        }

        title = lastWord
        save()
        tableView.reloadData()
    }
    
    @objc func newGame() {
        
        lastWord = allWords.randomElement() ?? "silkworm"
        title = lastWord
        usedWords.removeAll(keepingCapacity: true)
        save()
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        
        return cell
    }
    
    // MARK: - @objc funcs
    
    @objc func promptForAnswer() {
        let alert = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        alert.addTextField()
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [weak self, weak alert] _ in
            guard let answer = alert?.textFields?[0].text else { return }
            self?.submit(answer.lowercased())
        }
        
        alert.addAction(submitAction)
        present(alert, animated: true)
    }
    
    // MARK: - funcs for word's handling
    
    func submit(_ answer: String) {
        // test conditional breakpoint when answer has more than 6 letters
        let lowerAnswer = answer.lowercased()
        
        if isPossible(word: lowerAnswer) {
            if isOriginal(word: lowerAnswer) {
                if isReal(word: lowerAnswer) {
                    usedWords.insert(answer, at: 0)
                    
                    let indexPath = IndexPath(row: 0, section: 0)
                    tableView.insertRows(at: [indexPath], with: .automatic)
                    save()
                    
                    return
                } else {
                    showErrorMessage(title: "Word not recognized", message: "You can't just make them up!")
                }
            } else {
                showErrorMessage(title: "Word already used", message: "Be more original!")
            }
        } else {
            guard let title = title else { return }
            showErrorMessage(title: "Word not possible", message: "You can't spell that word from \(title.lowercased()).")
        }
    }
    
    func showErrorMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        present(alert, animated: true)
    }
    
    func isPossible(word: String) -> Bool {
        guard var tempWord = title?.lowercased() else { return false }
        
        for letter in word {
            if let position = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: position)
            } else {
                return false
            }
        }
        
        return true
    }
    
    func isOriginal(word: String) -> Bool {
        if word == title {
            return false
        } else {
            return !usedWords.contains(word)
        }
    }
    
    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count) // use it with apple's frameworks
        
        if range.length <= 3 {
            return false
        } else {
            let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
            return misspelledRange.location == NSNotFound
        }
    }
}
extension ViewController {
    func fetchWords() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                allWords = startWords.components(separatedBy: "\n")
            }
        }
    }
    
    func save() {
        let defaults = UserDefaults.standard
        defaults.set(lastWord, forKey: "word")
        defaults.set(usedWords, forKey: "usedWords")
    }
}
